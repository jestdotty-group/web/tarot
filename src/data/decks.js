export default {
	'default': [
		'new friendship, romance',
		'deepening attraction',
		'joy in company, friendship, celebration',
		'turning inwards, apathy',
		'loss, despair',
		'childhood nostalgia, good memories, an old friendship resumes',
		'daydreaming, wishful thinking, choices',
		'emotional detachment, leaving love behind, making a hard choice',
		'satisfaction, sensual pleasure, spiritual growth',
		'contentment, fulfilment, joy, family',

		'new insights, realizations',
		'failure to communicate',
		'miscommunication, misunderstanding',
		'recuperation, recovery, contemplation',
		'discord, dishonor, hollow victory',
		'moving on, travel, mentally getting to a better place',
		'lying, deceitfulness, theft, irresponsibility',
		'illusion of being trapped, powerlessness',
		'nightmares, problems, worry, guilt',
		'giving up, victimization, martyrdom',

		'new project, job, home, win',
		'juggling resources, waiting on results',
		'teamwork, improving skills',
		'miserliness, posessiveness',
		'loss of posessions or job or money',
		'giving or receiving money, a pay-rise, or obstaining resources',
		'reassessment, turning point, mild dissastisfaction',
		'paying attention to detail, focus, practice',
		'independence, self-reliance, increasing wealth',
		'great wealth, family property, inheritance',

		'new idea, business, action',
		'planning and preparation',
		'leadership, exploration',
		'a goal achieved, rest from action',
		'competition, disagreement, irritation',
		'victory, achievement, passing exams',
		'defense, conviction, strong belief',
		'organization, moving quickly, pregnancy',
		'continuing a battle, endurance, physical strength',
		'carrying burdens, responsibility, debt'
	]
}
