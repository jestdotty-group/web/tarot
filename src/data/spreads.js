export default {
	'fuckit': [
		'what is wrong?',
		'what should I do?',
		'what needs to fuck off?'
	],
	'wtf': [
		'what is real about this?',
		'what is wrong about this?',
		'where do I need more info?',
		'what can I do?'
	],
	'mind over matter': [
		'what is the worst that could happen?',
		'what will realistically happen?',
		'what traps me?',
		'what change can I make?'
	],
	'dragonfly': [
		'issue',
		'obstacle',
		'water: emotions',
		'fire: creativity',
		'earth: grounding',
		'air: attention',
		'outcome'
	],
	'character': [
		'central aspect',
		'central aspect',
		'past',
		'future',
		'private / unconscious',
		'public / conscious',
		'identity',
		'identity',
		'desire / fear',
		'desire / fear'
	],
	'mind body spirit': ['mind', 'body', 'spirit'],
	'past present future': ['past', 'present', 'future'],
	'situation obstacle lesson': ['situation', 'obstacle', 'lesson'],
	'embrace accept let go': ['embrace', 'accept', 'let go'],
	'dream life fear': ['dream', 'life', 'fear'],
	'tomorrow': ['tomorrow', 'next week', 'next month'],
	'desire obstacle solution': ['desire', 'obstacle', 'solution'],
	'red yellow green': ['red: stop', 'yellow: maybe', 'green: yes'],
}
